const sass = require('sass')
const {convertCss} = require('css-to-typestyle')
const { readFileSync } = require('fs')

/**
 * Usage:
 * node index.js ./path/to/my/style.scss
 * 
 * Once you copied the output in your tsx and fixed the 
 * bad things, you can search and replace uses of styles.myClass
 * by the new names with this regex
 * search: styles\.([^ }]+)
 * replaceBy: $1Class
 */

const [,, ...paths] = process.argv

const INDENT = 4

console.log(`Converting ${paths.length} sass files to typestyle`)

async function main() {
    for (let path of paths) {
        console.log(`\nTypestyle ${path}:`)
        const result = await readAndConvert(path)
        console.log(result)
    }
}

async function readAndConvert(path) {
    let data = readFileSync(path, {encoding: 'utf-8'})
    data = removeScope(data)

    const result = sass.renderSync({
        data,
        outputStyle: 'expanded'
    })
    
    let css = await convertCss(result.css.toString())
    css = classStyle(css)
    css = onlySingleQuotes(css)
    css = removeKeyQuotes(css)
    css = setIndent(css)
    css = removeSemicolon(css)
    css = replacePxString(css)
    return css
}

function removeScope(data) {
    return data.replace(/:(local|global)\(([^)]+)\)/g, '$2')
}

function classStyle(data) {
    data = data.replace(/cssRule\('[.#]([^'>[:]+)', /g, 'const $1Class = style(')
    data = data.replace(/^cssRule/g, '\n//Fix this rule manually\ncssRule')
    return data
}

function removeKeyQuotes(data) {
    return data.replace(/["'](.+)["']:/g, '$1:')
}

function setIndent(data) {
    return data.replace(/ {2,}/g, ''.padStart(INDENT, ' '))
}

function removeSemicolon(data) {
    return data.replace(/;/g, '')    
}

function onlySingleQuotes(data) {
    return data.replace(/"/g, '\'')
}

function replacePxString(data) {
    return data.replace(/(?!letterSpacing$): ["'](\d+)px["']/g, ': $1')
}

main()