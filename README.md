Usage:
node index.js ./path/to/my/style.scss

Once you copied the output in your tsx and fixed the 
bad things, you can search and replace uses of styles.myClass
by the new names with this regex
```
search: styles\.([^ }]+)
replaceBy: $1Class
```